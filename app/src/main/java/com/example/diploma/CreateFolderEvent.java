package com.example.diploma;

public class CreateFolderEvent {

    public final AddFolderDialog dialog;

    public CreateFolderEvent(AddFolderDialog dialog) {
        this.dialog = dialog;
    }


}
