package com.example.diploma;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class MainActivity extends AppCompatActivity {

    private FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fm = getSupportFragmentManager();
        setContentView(R.layout.main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try {
            EventBus.getDefault().register(this);
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(),Toast.LENGTH_LONG).show();
        }

        HomeFragment f = HomeFragment.newInstance("","");
        fm.beginTransaction()
                .replace(R.id.contentMain, f, "home")
                .addToBackStack("home")
                .commit();

        BottomNavigationView bNMIV=findViewById(R.id.bottom_navigation);

        bNMIV.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch (menuItem.getItemId()){
                    case R.id.home: {  HomeFragment f = HomeFragment.newInstance("","");
                        fm.beginTransaction()
                                .replace(R.id.contentMain, f, "home")
                                .addToBackStack("home")
                                .commit();
                    break;}
                    case R.id.favorite:{
                        FilesFragment f = FilesFragment.newInstance("photos");
                        fm.beginTransaction()
                                .replace(R.id.contentMain, f, "files")
                                .addToBackStack("files")
                                .commit();
                        break;
                    }
                }
                return true;
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_file_actions, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
//        Fragment f = fm.findFragmentById(R.id.contentMain);
//        if (f instanceof FilesFragment) {
//            ((FilesFragment) f).onPreviousFolderClick();
//        }
        fm.popBackStack();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FilesFragment f = (FilesFragment) fm.findFragmentById(R.id.contentMain);
        // TODO create callbacks for each action
        switch (item.getItemId()) {
            case R.id.createFolder:
                f.onClickAdd();
                break;
            case R.id.paste:
                f.onClickPaste();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateManagerEvent(ChangeFolderEvent event) {
        getSupportActionBar().setTitle(event.name);
    }
}
