package com.example.diploma;

public interface PreviousFolderListener {

    public void onPreviousFolderClick();
}
