package com.example.diploma;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FilesFragment extends Fragment implements PreviousFolderListener, MenuItemClickListener {

    private static final String FILES_TYPE = "files_type";

    private String filesType;

    private List<File> files = new ArrayList<>();
    private Context context;
    private FragmentManager fm;
    private FileAdapter adapter;
    private WeakReference<MainActivity> parentActivity;

    @BindView(R.id.rvFiles)
    RecyclerView rvFiles;

    private Unbinder unbinder;

    public FilesFragment() {
        // Required empty public constructor
    }

    public static FilesFragment newInstance(String filesType) {
        FilesFragment fragment = new FilesFragment();
        Bundle args = new Bundle();
        args.putString(FILES_TYPE, filesType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            filesType = getArguments().getString(FILES_TYPE);
        }
        context = getContext();
        fm = getFragmentManager();
        parentActivity = new WeakReference<>((MainActivity) getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_files, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        File f = getFile();//Environment.getRootDirectory();
        File[] tmp= f.listFiles();
        if(filesType.equals("photos")){
            List<File> tmpF=new ArrayList<File>();
            files = getPhotos(tmp,tmpF);
        } else{
            files = Arrays.asList(tmp);
        }
        adapter = new FileAdapter(files, f, fm);
        rvFiles.setLayoutManager(new LinearLayoutManager(context));
        rvFiles.setAdapter(adapter);
    }

    private List<File> getPhotos(File[] list, List<File> files){
        for (File fi: list) {
            if(fi.isDirectory()){
                getPhotos(fi.listFiles(), files);
            }
            else if(fi.getName().endsWith(".jpg") || fi.getName().endsWith(".jpeg") || fi.getName().endsWith(".png")){
                files.add(fi);
            }
        }
        return files;
    }

    private File getFile() {
        return new File("/sdcard");//getExternalStorageDirectory();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        unbinder = null;
        super.onDestroyView();
    }

    @Override
    public void onPreviousFolderClick() {
        adapter.callbackUpdate();
    }

    @Override
    public void onClickAdd() {
        adapter.addFolder();
    }

    @Override
    public void onClickPaste() {
        try {
            adapter.pasteDirectory();
        } catch (Exception e) {
            Log.d("paste", e.toString());
        }
    }
}
