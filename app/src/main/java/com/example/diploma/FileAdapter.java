package com.example.diploma;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FileAdapter extends RecyclerView.Adapter<FileAdapter.ViewHolder> {

    private List<File> files;
    private File currentDirectory;
    private FragmentManager fm;
    private File copyDir=null;

    public static final Comparator<File> COMPARE_BY_NAME = new Comparator<File>() {
        @Override
        public int compare(File lhs, File rhs) {
            return lhs.getName().compareTo(rhs.getName());
        }
    };

    public static final Comparator<File> COMPARE_BY_DATE = new Comparator<File>() {
        @Override
        public int compare(File lhs, File rhs) {
            return Math.round(lhs.lastModified() - rhs.lastModified());
        }
    };

    public static final Comparator<File> COMPARE_BY_TYPE = new Comparator<File>() {
        @Override
        public int compare(File lhs, File rhs) {
            int res=0;
            if(lhs.isDirectory()&&!rhs.isDirectory()){
                res= -1;
            }
            else if(rhs.isDirectory()&&!lhs.isDirectory()){
                res= 1;
            }
            return res;
        }
    };

    public FileAdapter(List<File> files, File curF, FragmentManager fm) {
        this.files=SortByNameType(files);
        this.currentDirectory = curF;
        EventBus.getDefault().post(new ChangeFolderEvent(currentDirectory.getAbsolutePath()));
        this.fm=fm;
    }

    @Override
    public void onViewAttachedToWindow(@NonNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        try {
            EventBus.getDefault().register(this);
        } catch (Exception e) {
            Log.d("bus_register", e.toString());
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(files.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateList(v, position);
                EventBus.getDefault().post(new ChangeFolderEvent(currentDirectory.getAbsolutePath()));
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ActionSelectDialog dialog = new ActionSelectDialog(v.getContext(), files.get(position), fm, position, new ChangeFileListener() {
                    @Override
                    public void OnClickDelete(int position) {
                        files.remove(position);
                        notifyItemRemoved(position);
                    }

                    @Override
                    public void OnClickCopy(File file) {
                        copyDir=file;
                    }
                });
                dialog.show(fm, "action_select");
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return files.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivIcon)
        AppCompatImageView ivIcon;

        @BindView(R.id.tvDescription)
        AppCompatTextView tvDescription;

        @BindView(R.id.tvDate)
        TextView tvDate;

        @BindView(R.id.tvSize)
        TextView tvSize;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(File file) {
            //ivIcon.setBackgroundDrawable();
            tvDescription.setText(file.getName());
            if(file.getName()==".."){
                tvSize.setText("");
                tvDate.setText("");
                ivIcon.setImageResource(R.drawable.ic_arrow_left);

            }
            else {
                Long lastModified = file.lastModified();
                String date = getFullTime(lastModified);
                tvDate.setText(date);
                if(file.isDirectory()){
                    int num=0;
                    if (file.listFiles()!=null){
                        num=file.listFiles().length;}
                    tvSize.setText(String.valueOf(num));
                    ivIcon.setImageResource(R.drawable.ic_folder);
                }
                else{
                    tvSize.setText(file.length()/1000+" КБайт");
                    ivIcon.setImageResource(R.drawable.ic_list_alt);
                }}
        }
    }

    public String getCurrentDirPath() {
        return currentDirectory.getAbsolutePath();
    }

    private void updateList(@NonNull View v, int position){
        File f=files.get(position);
        if (f.getName()==".."){
            currentDirectory = currentDirectory.getParentFile();
            FilesListChange();
        }
        else if (f.isDirectory()){
            currentDirectory=f;
            FilesListChange();
        }
        else {
            Intent i=new Intent(Intent.ACTION_VIEW);
            i.putExtra("file_name", f.getName());
            Intent i2=Intent.createChooser(i, "Открыть с помощью");
            v.getContext().startActivity(i2);
        }
    }

    public void callbackUpdate(){
        if(!currentDirectory.getName().equals("")){
            currentDirectory = currentDirectory.getParentFile();
            FilesListChange();
            EventBus.getDefault().post(new ChangeFolderEvent(currentDirectory.getAbsolutePath()));
        }
    }

    private void FilesListChange(){

        List<File> filesTmp=new ArrayList<>();
        if (currentDirectory.listFiles()!=null){
            for (File fi: currentDirectory.listFiles()) {
                filesTmp.add(fi);
            }
        }
        files = filesTmp;
        //SortBy(COMPARE_BY_TYPE, files);
        files=SortByNameType(files);
        if (currentDirectory.getName()!="/"&&currentDirectory.getParentFile()!=null) {
            files.add(0,new File(".."));
        }
        notifyDataSetChanged();
    }

    private void SortBy(Comparator<File> comparator, List<File> files){
        Collections.sort(files, comparator);
    }

    private List<File> SortByNameType(List<File> files){
        List<File> tmpDir=new ArrayList<>();
        List<File> tmpFile=new ArrayList<>();
        for (File f:files) {
            if(f.isDirectory()){
                tmpDir.add(f);
            }else tmpFile.add(f);
        }
        SortBy(COMPARE_BY_NAME, tmpDir);
        SortBy(COMPARE_BY_NAME, tmpFile);
        tmpDir.addAll(tmpFile);
        return tmpDir;
    }

    public void addFolder(){
        AddFolderDialog dialog = new AddFolderDialog();
        dialog.show(fm, "add_folder");
    }

    private static final String getFullTime(final long timeInMillis) {
        final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss");
        final Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timeInMillis);
        c.setTimeZone(TimeZone.getDefault());
        return format.format(c.getTime());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCreateManagerEvent(CreateFolderEvent event) {
        String newFolder= currentDirectory.getAbsolutePath()+"/"+((TextView)event.dialog.getDialog().findViewById(R.id.tvNewFileName)).getText().toString();
        File f=new File(newFolder);
        if(!f.exists())
            if(!f.mkdir()){
                Log.d("addFolder", "ok");
            }
            else
                Log.d("addFolder", "neok");
        files.add(f);
        files =SortByNameType(files);
        notifyDataSetChanged();
    }

    public void pasteDirectory()throws IOException{
        File sourceLocation;
        File targetLocation;
            if (copyDir!=null){
                sourceLocation=copyDir;
                targetLocation = new File( currentDirectory.getAbsolutePath()+"/"+copyDir.getName());}
            else return;
        pasteDirectory(sourceLocation, targetLocation);
        FilesListChange();
        copyDir=null;
    }

    public void pasteDirectory(File sourceLocation, File targetLocation) throws IOException {
        if (sourceLocation.isDirectory()) {
            if (!targetLocation.exists()) {
                targetLocation.mkdir();
            }
            String[] children = sourceLocation.list();
            for (int i = 0; i < sourceLocation.listFiles().length; i++) {
                pasteDirectory(
                        new File(sourceLocation, children[i]), new File(targetLocation, children[i]));
            }
        } else {
            InputStream in = new FileInputStream(sourceLocation);
            OutputStream out = new FileOutputStream(targetLocation); // Copy the bits from instream to outstream
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len); } in.close(); out.close();
        }
    }
}
