package com.example.diploma;

public interface MenuItemClickListener {

    void onClickAdd();

    void onClickPaste();

}
