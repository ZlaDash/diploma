package com.example.diploma;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import java.io.File;

public class ActionSelectDialog extends DialogFragment{

    private  String[] data= new String[]{"Копировать","Удалить"};
    private Context context;
    private File choiseFile;
    private FragmentManager fm;
    private int position;

    private ChangeFileListener changeListener;

    public ActionSelectDialog(Context context, File choiseFile, FragmentManager fm, int position, ChangeFileListener deleteListener) {
        this.context = context;
        this.choiseFile = choiseFile;
        this.fm = fm;
        this.position = position;
        this.changeListener = deleteListener;
    }

    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setItems(data, myItemsListener);
        return builder.create();
    }

    DialogInterface.OnClickListener myItemsListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case 0: {
                    changeListener.OnClickCopy(ActionSelectDialog.this.choiseFile);
                    break;
                }
                case 1:{
                    deleteRecursive(choiseFile);
                    changeListener.OnClickDelete(position);
                    /*if (choiseFile.exists()) {
                        if (choiseFile.delete()) {
                            Toast.makeText(context,choiseFile.getName()+" удален",Toast.LENGTH_LONG).show();
                            changeListener.OnClickDelete(position);
                        } else {
                            Toast.makeText(context,choiseFile.getName()+" не удален",Toast.LENGTH_LONG).show();
                        }
                    }*/
                    break;
                }
            }
        }

        public void deleteRecursive(File choiseFile) {
            if (choiseFile.isDirectory()) {
                for (File child : choiseFile.listFiles())
                    deleteRecursive(child);
                choiseFile.delete();
            } else choiseFile.delete();
        }
    };

}
