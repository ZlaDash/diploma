package com.example.diploma;

import java.io.File;

public interface ChangeFileListener {

    public void OnClickDelete(int position);

    public void OnClickCopy(File file);

}
